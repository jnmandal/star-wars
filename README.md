# star-wars

http://star-wars-challenge.s3-website-us-east-1.amazonaws.com/

## Building/Developing
* requires node.js v6.9.x, recommend using nvm if on unix machine
* requires yarn package manager, recommend installing via homebrew if on macOS
* certain parts of the app need secrets. these should come as environment variables or in a .env file (an example is included for your convenience)
* run `yarn install` then run `yarn start` to run a dev server
* run `yarn build` to build a production deploy

## Design Choices
data architecture
* since the poster images are not provided by SWAPI, I'm fetching them from a separate API, "The Movie DB". I configured a "List" there which is basically just a collection of movie objects. in order to make sure we are connecting the data correctly, movies are sorted by release date before stored in state. the fetches for both the SWAPI film list and the TMDB poster list obviously happen concurrently so we have to handle either one loading before the other

technology
* I am using create-react-app to scaffold the project because its fast and easy. I prefer using CSS modules so that classNames can be modified and analyzed at build time but that type of buildchain is unnecessary for a project of this size anyways so it does not warrant the configuration at this time.
* I am using redux to construct and manage the state tree for the app. Its questionable whether we need it for an app of this size, but it makes testing simpler and its always nice to have the redux devtools debugger around
* for the bar chart I am using victory because it is a cross platform charting library (technically anything based on D3 can be ported between react-native and react but victory is especially easy to use) this maximizes code reusbaility
