import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
// reducers
import films from './Films';
import people from './People';

const enhancers = [
  applyMiddleware(thunk)
];

// allow browser extension to serve as a 'middleware' and inspect state
// depending on need for obfuscation/security, we might not want to do this
if (window.devToolsExtension) {
  enhancers.push(window.devToolsExtension());
}

const reducer = combineReducers({ films, people });

const store = createStore(reducer, compose(...enhancers));

export default store;
