import { getPerson } from '../api';

const FETCH_PERSON = 'FETCH_PERSON';
const FETCH_PERSON_SUCCESS = 'FETCH_PERSON_SUCCESS';
const FETCH_PERSON_FAILURE = 'FETCH_PERSON_FAILURE';

export default (state = {}, {type, personId, person, error}) => {
  switch (type) {
    case FETCH_PERSON:
      return { ...state, [personId]: { requestInProgress: true }};
    case FETCH_PERSON_SUCCESS:
      return { ...state, [personId]: { requestInProgress: false, ...person }};
    case FETCH_PERSON_FAILURE:
      return { ...state, [personId]: { requestInProgress: false, error }};
    default:
      return state;
  }
}

export const beginFetchPerson = (personId) =>
  ({ type: FETCH_PERSON, personId})

export const fetchPersonSuccess = (personId, person) =>
  ({ type: FETCH_PERSON_SUCCESS, personId, person })

export const fetchPersonFailure = (personId, error) =>
  ({ type: FETCH_PERSON_FAILURE, personId, error })

export const fetchPerson = (personId) =>
  (dispatch, getState) => {
    // if person data is already loaded or loading, don't reload
    if (getState().people[personId]) return getState().people[personId];
    dispatch(beginFetchPerson(personId));
    getPerson(personId)
      .then(person => dispatch(fetchPersonSuccess(personId, person)))
      .catch(error => dispatch(fetchPersonFailure(personId, error)))
  }
