import Reducer, {
  fetchFilmsSuccess,
  fetchFilmPostersSuccess } from './Films';

import films from '../fixtures/films';
import posterList from '../fixtures/poster_list';

it('loads films', () => {
  const state = Reducer({}, fetchFilmsSuccess(films));
  expect(state.list).toBeDefined();
  expect(state.list).toHaveLength(7);
});

it('sorts loaded films', () => {
  const state = Reducer({}, fetchFilmsSuccess(films));
  const outOfOrderFilms = [ films.results[6], ...(films.results.slice(0,6)) ]
  expect(state.list[0].title).toEqual('A New Hope');
});

it('loads posters', () => {
  const state = Reducer({}, fetchFilmPostersSuccess(posterList));
  expect(state.list).toBeDefined();
  expect(state.list).toHaveLength(7);
});

it('sorts loaded posters', () => {
  const state = Reducer({}, fetchFilmPostersSuccess(posterList));
  const outOfOrderFilms = [ films.results[6], ...(films.results.slice(0,6)) ]
  expect(state.list[0].poster_path).toEqual('/tvSlBzAdRE29bZe5yYWrJ2ds137.jpg');
});


it('properly loads films then posters', () => {
  const state = Reducer(Reducer({}, fetchFilmsSuccess(films)),
                        fetchFilmPostersSuccess(posterList));

  expect(state.list).toBeDefined();
  expect(state.list).toHaveLength(7);
  expect(state.list[0].title).toEqual('A New Hope');
  expect(state.list[0].poster_path).toEqual('/tvSlBzAdRE29bZe5yYWrJ2ds137.jpg');
});

it('properly loads posters then films', () => {
  const state = Reducer(Reducer({}, fetchFilmPostersSuccess(posterList)),
                        fetchFilmsSuccess(films));

  expect(state.list).toBeDefined();
  expect(state.list).toHaveLength(7);
  expect(state.list[0].title).toEqual('A New Hope');
  expect(state.list[0].poster_path).toEqual('/tvSlBzAdRE29bZe5yYWrJ2ds137.jpg');
});
