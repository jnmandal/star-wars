import { getFilms, getPosterList } from '../api';

const FETCH_FILMS = 'FETCH_FILMS';
const FETCH_FILMS_SUCCESS = 'FETCH_FILMS_SUCCESS';
const FETCH_FILMS_FAILURE = 'FETCH_FILMS_FAILURE';

const FETCH_FILM_POSTERS = 'FETCH_FILM_POSTERS';
const FETCH_FILM_POSTERS_SUCCESS = 'FETCH_FILM_POSTERS_SUCCESS';
const FETCH_FILM_POSTERS_FAILURE = 'FETCH_FILM_POSTERS_FAILURE';

export default (state = {}, {type, films, error, posterList}) => {
  switch (type) {
    case FETCH_FILMS:
      return { ...state, filmsRequestInProgress: true };
    case FETCH_FILMS_SUCCESS:
      const list = films.results.sort((film1, film2) =>
          film1.release_date > film2.release_date);
      if (state.list && state.list.length > 0) return {
          ...state, filmsRequestInProgress: false,
          list: state.list.map((film, index) => ({...film, ...list[index]}))
        };
      return { ...state, filmsRequestInProgress: false, list };
    case FETCH_FILMS_FAILURE:
      return { ...state, filmsRequestInProgress: false, error };
    case FETCH_FILM_POSTERS:
      return { ...state, postersRequestInProgress: true };
    case FETCH_FILM_POSTERS_SUCCESS:
      const posters = posterList.results
        .sort((film1, film2) => film1.release_date > film2.release_date);
      if (state.list && state.list.length > 0) {
        return { ...state, postersRequestInProgress: false,
          list: state.list.map((film, index) => ({...film, poster_path: posters[index].poster_path})) };
      }
      return { ...state,
        postersRequestInProgress: false,
        list: posters.map(film => ({poster_path: film.poster_path})) };
    case FETCH_FILM_POSTERS_FAILURE:
      return { ...state, postersRequestInProgress: false, error };
    default:
      return state;
  }
}

export const beginFetchFilms = () =>
  ({ type: FETCH_FILMS })

export const fetchFilmsSuccess = (films) =>
  ({ type: FETCH_FILMS_SUCCESS, films })

export const fetchFilmsFailure = (error) =>
  ({ type: FETCH_FILMS_FAILURE, error })

export const fetchFilms = () =>
  (dispatch) => {
    dispatch(beginFetchFilms());
    getFilms()
      .then(films => dispatch(fetchFilmsSuccess(films)))
      .catch(error => dispatch(fetchFilmsFailure(error)));
  }

export const beginFetchFilmPosters = () =>
  ({ type: FETCH_FILM_POSTERS })

export const fetchFilmPostersSuccess = (posterList) =>
  ({ type: FETCH_FILM_POSTERS_SUCCESS, posterList })

export const fetchFilmPostersFailure = (error) =>
  ({ type: FETCH_FILM_POSTERS_FAILURE, error })

export const fetchFilmPosters = (posterListId) =>
  (dispatch) => {
    dispatch(beginFetchFilmPosters());
    getPosterList(posterListId)
      .then(posterList => dispatch(fetchFilmPostersSuccess(posterList)))
      .catch(error => dispatch(fetchFilmPostersFailure(error)));
  }

export const fetchFilmsAndPosters = (posterListId) =>
  (dispatch, getState) => {
    fetchFilms(posterListId)(dispatch, getState);
    fetchFilmPosters(posterListId)(dispatch, getState);
  }
