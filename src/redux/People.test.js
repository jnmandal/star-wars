import Reducer, {
  fetchPersonSuccess,
  fetchPerson
} from './People';

import person from '../fixtures/person';

it('loads a person', () => {
  const state = Reducer({}, fetchPersonSuccess(3, person));
  expect(state[3]).toBeDefined();
});

it('does not load a person twice', () => {
  const state1 = Reducer({}, fetchPersonSuccess(3, person));
  const dispatch = (action) => Reducer(state1, action);
  const getState = () => ({people: state1});
  expect(fetchPerson(3)(dispatch, getState)).toBe(state1[3]);
})
