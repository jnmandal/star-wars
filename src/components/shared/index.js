import React from 'react';
import './index.css';

export const Card = (props) => (
  <div className="Card">
    { props.children }
  </div>
);

export const Columns = (props) => (
  <div className="Columns">
    {props.children}
  </div>
);

export const ArticleHeader = ({title}) => (
  <header className="ArticleHeader">{title}</header>
)
