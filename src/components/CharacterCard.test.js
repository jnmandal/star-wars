import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import CharacterCard from './CharacterCard';

import person from '../fixtures/person';
import films from '../fixtures/films';

it('renders without crashing with person data', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CharacterCard {...person} movies={films.results} />, div);
});

it('renders correctly w/ person data', () => {
  const tree = renderer.create(<CharacterCard {...person} movies={films.results} />).toJSON();
  expect(tree).toMatchSnapshot();
});
