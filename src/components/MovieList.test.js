import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import MovieList from './MovieList';

import films from '../fixtures/films';
import person from '../fixtures/person';

const peopleStub = { 1: person }

it('will render without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MovieList
      movies={films.results}
      people={peopleStub}
      fetchPerson={() => null}/>, div);
});

it('renders correctly w/ film data', () => {
  const tree = renderer.create(
    <MovieList
      movies={films.results}
      people={peopleStub}
      fetchPerson={() => null} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
