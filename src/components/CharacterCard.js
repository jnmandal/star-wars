import React from 'react';
import { Card, ArticleHeader } from './shared';

const CharacterCard = (props) => (
  <Card>
    <article className="CharacterDescription">
      <ArticleHeader title={props.name} />
      <p>height: {props.height} cm</p>
      <p>born: {props.birth_year}</p>
      <div>
        appears in:
        <ul>
          {
            props.films &&
            props.films.map(filmUrl => {
              const filmId = filmUrl.split("/").slice(-2)[0] - 1;
              return props.movies[filmId] ?
                <li key={filmId}>{props.movies[filmId].title}</li> : <li>film {filmId}</li>;
            })
          }
        </ul>
      </div>
    </article>
  </Card>
)

export default CharacterCard
