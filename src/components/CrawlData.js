import React from 'react';
import { VictoryChart, VictoryBar, VictoryAxis } from 'victory';
import { Card, ArticleHeader } from './shared';

const CrawlData = (props) => {
  return (
    <Card>
      <article>
        <ArticleHeader title={'How long are the openening crawls?'} />
        { props.movies.length > 0 && props.movies[0].opening_crawl ?
            <VictoryChart>
              <VictoryAxis
                dependentAxis />
              <VictoryAxis
                tickFormat={(datum) => `${datum}`} />
              <VictoryBar
                horizontal={true}
                data={[...props.movies].reverse()}
                x={datum => `ep. ${datum.episode_id}`}
                y={datum => datum.opening_crawl.split(/\s/).length} />
            </VictoryChart> : "Loading"
         }
       </article>
    </Card>
  )
}

export default CrawlData
