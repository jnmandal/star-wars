import React from 'react';
import './MovieList.css';
import { Card, Columns, ArticleHeader } from './shared';

const MovieListDescription = (props) => (
  <article className="MovieList__description">
    <ArticleHeader title={props.title} />
    <img className="MovieList__description__image"
      alt={`film poster for ${props.name}`}
      src={props.poster_path ? `https://image.tmdb.org/t/p/w300/${props.poster_path}`: 'http://placehold.it/300x500'}/>
    <p>
      directed by {props.director}
    </p>
    <ul>
      {
        props.characters &&
        props.characters.slice(0,3).map(personUrl => {
          const personId = personUrl.split("/").slice(-2)[0];
          // TODO move this logic elsewhere
          if (!props.people[personId]) props.fetchPerson(personId);
          return (
            <li key={personUrl}>
              {props.people[personId] ?
                  props.people[personId].name :
                  `character ${personId}`}
            </li>
          )
        })
      }
    </ul>
  </article>
)

const MovieList = (props) => {
  const movies = props.movies.reduce((matrix, nextMovie, index) => {
    let newMatrix = (index % 2 === 0) ? [...matrix, []] : [ ...matrix ];
    newMatrix[newMatrix.length - 1].push(
      <MovieListDescription
        key={index}
        {...nextMovie}
        people={props.people}
        fetchPerson={props.fetchPerson} />
    );
    return newMatrix;
  }, []);
  return (
    <ul className="MovieList">
      {
        movies.map((movieSet, i) => (
          <li key={i} >
            <Card>
              <Columns>
                { movieSet }
                { movieSet.length % 2 !== 0 &&
                    <div className="MovieList__description" /> }
              </Columns>
            </Card>
          </li>
        ))
      }
    </ul>
  );
}

export default MovieList;
