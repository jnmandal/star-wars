import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';

import CrawlData from './CrawlData';
import films from '../fixtures/films';

it('will render without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CrawlData movies={films} />, div);
});

it('renders correctly w/ film data', () => {
  const tree = renderer.create(<CrawlData movies={films} />).toJSON();
  expect(tree).toMatchSnapshot();
});
