export const request = (url, { method = 'GET', headers = {} } = {}) =>
  fetch(url, { method,
               headers: {'Content-Type': 'application/json', ...headers}})
    .then(res => res.json())

// normally the default fallback would be a staging environment, so a sandbox doesnt accidentally point to production
const SWAPI_BASE_URL = process.env.REACT_APP_SWAPI_BASE_URL || '//swapi.co/api';

export const getFilms = () =>
  request(`${SWAPI_BASE_URL}/films/`);

export const getPerson = (personId) =>
  request(`${SWAPI_BASE_URL}/people/${personId}/`);

// normally the default fallback would be a staging environment, so a sandbox doesnt accidentally point to production
const TMDB_BASE_URL = process.env.REACT_APP_TMDB_BASE_URL || '//api.themoviedb.org/4';
const TMDB_API_KEY = process.env.REACT_APP_TMDB_API_KEY;

export const getPosterList = (listId) =>
  request(`${TMDB_BASE_URL}/list/${listId}?api_key=${TMDB_API_KEY}`);
