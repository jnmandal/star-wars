import React, { Component } from 'react';
import './App.css';
import CharacterCard from './components/CharacterCard';
import CrawlData from './components/CrawlData';
import MovieList from './components/MovieList';
// app config
import settings from './settings.json';
// state management
import { connect } from 'react-redux';
import { fetchPerson } from './redux/People';
import { fetchFilmsAndPosters } from './redux/Films';

// TODO
// add tmdb attribution
// redux tests

class App extends Component {
  componentDidMount() {
    this.props.fetchFilmsAndPosters(settings.moviePosterListId);
    this.props.fetchPerson(settings.favoriteCharacters.love);
    this.props.fetchPerson(settings.favoriteCharacters.hate);
  }
  render() {
    const loveChar = this.props.people[settings.favoriteCharacters.love] ?
      this.props.people[settings.favoriteCharacters.love] :
      {id: settings.favoriteCharacters.love};
    const hateChar = this.props.people[settings.favoriteCharacters.hate] ?
      this.props.people[settings.favoriteCharacters.hate] :
      {id: settings.favoriteCharacters.hate};

    return (
      <main className="App">
        <section className="App__side-panel">
          <CharacterCard
            {...loveChar}
            movies={this.props.films} />
          <CharacterCard
            {...hateChar}
            movies={this.props.films} />
          <CrawlData movies={this.props.films} />
        </section>
        <section className="App__center-panel">
          {
            this.props.films.length > 0 ?
              <MovieList
                fetchPerson={this.props.fetchPerson}
                people={this.props.people}
                movies={this.props.films} /> : <div>Loading</div>
          }
        </section>
      </main>
    );
  }
}

const stateToPros = (state) =>
  ({
    films: state.films.list || [],
    people: state.people
  })

const dispatchToProps = (dispatch) =>
  ({
    fetchFilmsAndPosters: (posterListId) => dispatch(fetchFilmsAndPosters(posterListId)),
    fetchPerson: (personId) => dispatch(fetchPerson(personId))
  })

export default connect(stateToPros, dispatchToProps)(App);
